from random import choice, randint


def bad_generator():
    total = 0
    
    while True:
        if total in range(90, 95, 1):
            value = choice([randint(1, 3), randint(8, 10)])
        else:
            value = randint(1, 10)
        
        yield value

        total += value

def true_generator():
    while True:
        yield randint(1, 10)


def game(generator):
    gen = generator()

    return next(gen)


def prob(generator, n=10_000):
    m = 0
    for _ in range(n):
        m += game(generator=generator)

    return m/n


if __name__ == "__main__":
    print("true prob:", prob(generator=true_generator))
    print("bad prob:", prob(generator=bad_generator))
