from datetime import datetime
import random
import time
import requests


class BadRequest(Exception):
    def __str__(self):
        return f"Bad Request"


def send_request(url: str) -> str:
    print('Send request ...')
    response = requests.get(url)

    if not response.ok or random.randint(0, 1):
        print('Bad request!')
        raise BadRequest('Bad request url!')

    return response.content


def post_processing(data: str) -> str:
    random_seconds = random.randint(1, 4)
    print(f'Post-processing {random_seconds} ...')

    time.sleep(random_seconds)
    return data


class DynamicSleep:
    def __init__(self, seconds):
        self.seconds = seconds 

    def __enter__(self):
        self.start = datetime.now().second

    def __exit__(self, *exc_info):
        if (datetime.now().second - self.start) >= self.seconds:
            return True
        
        time.sleep(self.seconds)

        return True


if __name__ == "__main__":
    base_url = 'https://books.toscrape.com/catalogue/page-%s.html'
    for p in range(1, 10 + 1):
        with DynamicSleep(seconds=4):
            url = base_url % p
            data = send_request(url=url)
            data = post_processing(data=data)

