
class Value:
    def __init__(self) -> None:
        self.__amount = 0
    
    def __set__(self, instance, value):
        if not isinstance(value, int):
            raise TypeError("Amount can only be an integer")
        
        if value < 0:
            raise ValueError("Amount can never be less than zero")
            
        instance.__dict__[self.__amount] = value    
    
    def __get__(self, instance, owner):
        return int(instance.__dict__[self.__amount] * (1 - instance.commission))
        


class Operation:
    """
    >>> new_account = Operation(0.3)
    >>> new_account.amount = 100
    >>> new_account.amount
    70

    >>> new_account = Operation(0.5)
    >>> new_account.amount = 50
    >>> new_account.amount
    25
    """
    amount = Value()

    def __init__(self, commission):
        self.commission = commission


if __name__ == "__main__":
    import doctest
    doctest.testmod()
