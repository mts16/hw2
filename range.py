import pytest


class RangeIterator:
    def __init__(self, start: int, stop: int, step: int) -> None:
        self.start = start
        self.stop = stop
        self.step = step
        
    def __iter__(self) -> 'RangeIterator':
        return self

    def __next__(self):
        if self.start < self. stop:
            result = self.start
            self.start += self.step
            return result
        raise StopIteration


class MyRange:
    def __init__(self, *args) -> None:
        if args.__len__() == 1:
            self.stop = args.__getitem__(0)
            self.start = 0
            self.step = 1
        
        if args.__len__() == 2:
            self.start = args.__getitem__(0)
            self.stop = args.__getitem__(1)
            self.step = 1

        if args.__len__() == 3:
            self.start = args.__getitem__(0)
            self.stop = args.__getitem__(1)
            self.step = args.__getitem__(2)

    def __iter__(self) -> RangeIterator:
        return RangeIterator(start=self.start, stop=self.stop, step=self.step)

    def __eq__(self, __o: object) -> bool:
        if not isinstance(__o, MyRange):
            return False
        
        if __o.start == self.start and __o.stop == self.stop and __o.step == self.step:
            return True

        return False

    def __repr__(self) -> str:
        return f"MyRange({self.start}, {self.stop}, {self.step})"

    def __len__(self) -> int:
        return (self.stop - self.start - 1) // self.step + 1

    def __contains__(self, item) -> bool:
        if item < self.start or item > self.stop - self.start:
            return False

        if self.step == 1:
            return True
        
        if item % 2 == 0 and (self.start + self.step) % 2 == 0:
            return True
        
        if item % 2 > 0 and (self.start + self.step) % 2 > 0:
            return True

        return False

    def __getitem__(self, key):
        if (self.__len__() - 1) < key or key < 0:
            raise IndexError("range object index out of range")
        
        return self.step * key + self.start


def test_getitem():
    myRange = MyRange(10)
    assert myRange[5] == 5

    with pytest.raises(IndexError) as excinfo:
        myRange[15]
    
    assert excinfo.value.args[0] == "range object index out of range"

def test_contains():
    assert MyRange(20).__contains__(10) is True
    assert MyRange(1, 20, 2).__contains__(10) is False
    assert MyRange(1, 20, 3).__contains__(10) is True
    

def test_len():
    assert len(MyRange(20)) == 20
    

def test_eq():
    firstRange = MyRange(10)
    secondRange = MyRange(30)
    thirdRange = MyRange(10)

    assert (firstRange == secondRange) is False
    assert (firstRange == thirdRange) is True

    firstRange = MyRange(1, 20, 4)
    secondRange = MyRange(1, 30, 3)
    thirdRange = MyRange(1, 20, 4)

    assert (firstRange == secondRange) is False
    assert (firstRange == thirdRange) is True