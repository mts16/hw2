
import os
from tempfile import TemporaryFile
from typing import List
import csv
import pytest

class Product:
    """Обертка для товара, у которого есть название и цена."""
    def __init__(self, name: str, price: float) -> None:
        if len(name) == 0:
            raise ValueError("Name not be empty")
        if price < 0: 
            raise ValueError("Price not be less 0")

        self.name = name
        self.price = price

    def __repr__(self) -> str:
        return f"name: {self.name}, price: {self.price}"

    def __str__(self) -> str:
        return f"goods '{self.name}' at a price of {self.price}"
    
    def __eq__(self, __o: object) -> bool:
        return self.price == __o.get_price() and self.name == __o.get_name()

    def change_price(self, percent: float):
        if percent < 0:
            raise ValueError("Percent not be 0")
        
        self.price *= percent
        
    def get_price(self) -> float:
        return self.price
    
    def get_name(self) -> str:
        return self.name

def test_change_price():
    product = Product("test", 20)

    assert product.get_name == "test"
    assert product.get_price == 20

    product.change_price(0.2)
    assert product.get_price == 4

class ProductCount(Product):
    """Обертка, которая учитывает количество товара"""
    def __init__(self, name: str, price: float, count: int) -> None:
        super().__init__(name, price)

        if count < 0:
            raise ValueError("Count not be less 0")

        self.count = count

    def __repr__(self) -> str:
        return f"name: {self.name}, price: {self.price}, count: {self.count}"

    def __str__(self) -> str:
        return f"goods '{self.name}' at a price of {self.price} in stock {self.count}"

    def __eq__(self, __o: object) -> bool:
        return super().__eq__(__o) and self.count == __o.get_count()

    def add(self, count: int):
        if count < 0:
            raise ValueError("Count not be less 0")
        
        self.count += count

    def remove(self, count: int):
        if count < 0:
            raise ValueError("Count not be less 0")
        
        if count > self.count:
            raise ValueError(f"Count should be less than {self.count}")

        self.count -= count
    
    def get_count(self) -> int:
        return self.count


def test_product_count():
    product_count = ProductCount("test", 20, 10)

    assert product_count.get_name() == "test"
    assert product_count.get_price() == 20
    assert product_count.get_count() == 10

    product_count.add(5)
    assert product_count.get_count() == 15

    product_count.remove(5)
    assert product_count.get_count() == 10


@pytest.fixture
def tempfile():
    with TemporaryFile(mode='w') as f:
        yield f

class ListProducts:
    """Обертка набора товаров."""
    def __init__(self, products: List[ProductCount]) -> None:
        self.products = products

    def from_file(self, path: str):
        """
        Информация о доставке товара может находиться в файле вида:
        name,price,count
        Apple,20.3,5
        Orange,30.5,10
        """
        with open(path, "r") as csv_file:
            reader = csv.reader(csv_file)
            next(reader, None)

            for row in reader:
                self.products.append(ProductCount(row[0], float(row[1]), int(row[2])))

    def __contains__(self, product: Product) -> bool:
        for elem in self.products:
            if elem.__eq__(product):
                return True
        
        return False

    def __getitem__(self, name):
        """узнать количество товара по названию товара"""
        for product in self.products:
            if product.get_name() == name:
                return product.get_count()
    
    def __len__(self) -> int:
        count = 0
        for _ in self.products:
            count += 1
        
        return count

    def add(self, product: ProductCount):
        self.products.append(product)

    def remove(self, product: ProductCount):
        self.products.remove(product)
    
    def get_products(self) -> list[ProductCount]:
        return self.products

def test_list_products():
    productCounts = [None]
    productCounts.append(ProductCount("test_1", 10, 2))
    productCounts.append(ProductCount("test_2", 15.2, 7))

    products = ListProducts(productCounts)
    assert productCounts == products.get_products()

    products.remove(ProductCount("test_1", 10, 2))

    assert len(products) == 1

def test_list_products_with_file(tempfile):
    tempfile.write('name,price,count')
    tempfile.write('Apple,20.3,5')
    tempfile.write('Orange,30.5,10')
    tempfile.seek(0)

    products = ListProducts(None)
    products.from_file(os.path.dirname(tempfile.name))

    assert len(products) == 2


class Shop:
    """Обертка для магазина"""
    def __init__(self, name: str) -> None:
        if len(name) == 0:
            raise ValueError("Name not be empty")
        
        self.name = name
        self.products = [None]

    def __containts__(self, product: Product):
        for elem in self.products:
            if elem.__eq__(product):
                return True
        return False

    def increase_prices(self, percent: float):
        """увеличить цену всех товаров на какой-то процент"""
        if percent < 1:
            raise ValueError("Percent not be less 1")

        self.__change_price(percent)

    def decrease_prices(self, percent: float):
        """увеличить цену всех товаров на какой-то процент"""
        if percent > 1:
            raise ValueError("Percent not be greater 1")

        self.__change_price(percent)

    def __change_price(self, percent: float):
        for product in self.products:
            product.change_price(percent)

    def add_product(self, product: ProductCount):
        self.products.append(product)

    def add_products(self, products: ListProducts):
        self.products.extend(products.get_products())

    def sell_product(self, product: ProductCount):
        self.products.remove(product)

    def sell_products(self, products: ListProducts):
        self.products = [elem for elem in self.products if (elem not in products)]